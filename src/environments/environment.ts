// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDfsZTaSxQVyy_CRNqVGIowKXktW0MELYg",
    authDomain: "oshop-6bb3e.firebaseapp.com",
    databaseURL: "https://oshop-6bb3e.firebaseio.com",
    projectId: "oshop-6bb3e",
    storageBucket: "",
    messagingSenderId: "792834279381"
  }
};
